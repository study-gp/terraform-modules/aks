## [1.3.2](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.3.1...v1.3.2) (2022-02-28)


### Bug Fixes

* **ci:** try to add VERSION.txt file from CI ([89ddd6e](https://gitlab.com/study-gp/terraform-modules/aks/commit/89ddd6ef70aa25370c436cc692a16aac4d567fd6))

## [1.3.1](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.3.0...v1.3.1) (2022-02-28)


### Bug Fixes

* **ci:** add commit message ([5f51691](https://gitlab.com/study-gp/terraform-modules/aks/commit/5f51691e7f495852d2738a3b3669dc527d9f878b))

# [1.3.0](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.2.3...v1.3.0) (2022-02-28)


### Features

* **gitlab-ci:** add version.txt from CI ([d0c9bc1](https://gitlab.com/study-gp/terraform-modules/aks/commit/d0c9bc170e9b8f1f3229917d3b10b27a572248bf))

## [1.2.3](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.2.2...v1.2.3) (2022-02-28)

## [1.2.2](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.2.1...v1.2.2) (2022-02-15)

## [1.2.1](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.2.0...v1.2.1) (2022-02-08)


### Bug Fixes

* **terraform-module:** change terraform module name ([420113f](https://gitlab.com/study-gp/terraform-modules/aks/commit/420113fbde9bbd72b17f0dfb029db7d6f9c7b972))

# [1.2.0](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.1.1...v1.2.0) (2022-02-08)


### Bug Fixes

* **terraform-module:** upload terraform module to the gitlab registry ([d04959f](https://gitlab.com/study-gp/terraform-modules/aks/commit/d04959f558dec9fd49a8555a7570bad6fdcd783b))


### Features

* **terraform-module:** upload terraform module to the gitlab registry ([d627d52](https://gitlab.com/study-gp/terraform-modules/aks/commit/d627d52097e913fd798b9f0dc232de65f8bcbef2))

## [1.1.1](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.1.0...v1.1.1) (2022-02-08)

## [1.2.1](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.2.0...v1.2.1) (2021-08-26)


### Bug Fixes

* **git:** apply merge from main ([fc300f4](https://gitlab.com/study-gp/terraform-modules/aks/commit/fc300f4fdfd152d9593d0d3b0597e58f60ac6293))

# [1.2.0](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.1.2...v1.2.0) (2021-08-25)


### Features

* **pbi00114:** some test\n\nSome test feat ([d0a6304](https://gitlab.com/study-gp/terraform-modules/aks/commit/d0a6304760951b06e2715115b8b03a0f67b2efe0))

## [1.1.2](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.1.1...v1.1.2) (2021-08-25)

## [1.1.1](https://gitlab.com/study-gp/terraform-modules/aks/compare/v1.1.0...v1.1.1) (2021-08-25)
